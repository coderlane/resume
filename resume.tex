% Travis Lane's Resume
% We need PERSONAL_PHONE and PERSONAL_EMAIL set to compile. I value
% my privacy and would prefer to avoid publishing them in raw text.

%\documentclass{resume}
\documentclass{article}

% Packages
\usepackage{xparse}
\usepackage{enumitem}
\usepackage{geometry}
\usepackage[hidelinks]{hyperref}
\usepackage{ifthen}
\usepackage{nopageno}
\usepackage{titlesec}

% A4 is close enough to 8.5x11 and looks better IMHO
\geometry{a4paper, %
	left=4em, %
	right=4em, %
	top=4em, %
	bottom=4em %
}

% noindent
\setlength\parindent{0pt}

% \getenv
\ExplSyntaxOn
\NewDocumentCommand{\getenv}{om}
 {
  \sys_get_shell:nnN { kpsewhich ~ --var-value ~ #2 } { } \l_tmpa_tl
  \tl_trim_spaces:N \l_tmpa_tl
  \IfNoValueTF { #1 }
   {
    \tl_use:N \l_tmpa_tl
   }
   {
    \tl_set_eq:NN #1 \l_tmpa_tl
   }
 }
\ExplSyntaxOff

\newcommand{\uhref}[2]{\href{#1}{\underline{\smash{#2}}}}

% Section formatting
\titleformat{\section}{\Large}{-2em}{}{}[\titlerule]
\titlespacing{\section}{0pt}%
	{4pt plus 2pt minus 2pt}{6pt plus 4pt minus 2pt}
\titlespacing{\subsection}{0pt}%
	{4pt plus 2pt minus 2pt}{4pt plus 4pt minus 2pt}

% Custom position macro
\newenvironment{position}[3] {
	% Position                       (Optional) Team         Date
	{\bf #1} \ifthenelse{\equal{#2}{}}{}{\bf -- #2} \hfill {\bf #3}
	\vspace{-.8em}
	\normalfont
	\begin{itemize}
		\itemsep -0.25em  \vspace{-0.75em}
		\item[]
}{
	\end{itemize}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}

% Header, print my name and contact info
\begin{center}
	{\LARGE \textbf{Travis Lane}}

	\getenv{EMAIL}
	\\
  \uhref{https://coderlane.com}{{coderlane.com}} \textbullet \ \
	\uhref{https://www.linkedin.com/in/coderlane/}{{linkedin.com/in/coderlane}}
\end{center}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{Professional Experience}

\subsection*{Google}
\begin{position}{Software Engineer}%
	{Google Drive File Stream}{May 2019 -- Present}
	\item Designed and implemented hidden file support for MacOS and Windows
	\item Reduced overall CPU utilization by 5\% and notification handling by 85\%
		through profiling with DTrace and Perf
	\item Implemented a proof of concept migration to HTTP/2 to further reduce
		resource utilization
\end{position}

\subsection*{Dell EMC Isilon}
\begin{position}{Principal Software Engineer}%
	{Cluster Infrastructure}{October 2018 -- May 2019}
	\item Extended inode format to enable implementation of
		\uhref{https://www.dellemc.com/resources/en-us/asset/white-papers/products/storage/h17549_wp_isilon_filesystem_compression.pdf}{{inline filesystem compression}}
	\item Enabled userspace cross compilation of inode format library and
		implemented unit tests to ensure the quality of future changes
\end{position}
\begin{position}{Senior Software Engineer}%
	{Cluster Infrastructure}{October 2016 -- October 2018}
	\item Modified distributed locking service to reduce time required for
		system upgrades by up to eightfold
	\item Designed and implemented a proof of concept throttle for a
		job execution service to better handle dynamic load
	\item Replaced cluster configuration daemon with one based on previously
		implemented Paxos consensus library
\end{position}
\begin{position}{Software Engineer}%
	{Cluster Infrastructure}{May 2015 -- October 2016}
	\item Designed and implemented an internal consensus library based on the
		Paxos consensus protocol to manage cluster configurations capable of
		supporting over 500 participants
	\item Developed enhancements and bug fixes for a variety of kernel and
		userspace subsystems including: distributed locking service,
		kernel RPC service, group management protocol,
		and cluster configuration daemon
\end{position}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{Technology Summary}

\begin{description}[leftmargin=!,labelwidth=13em] \itemsep 0em
\item[Languages]
	C, C++, Go, and Python
\item[Interests]
	File Systems, Networking, Distributed Systems, and Computer Security
\end{description}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{Side Projects}

\begin{position}{Minecraft Sidecart}{}{July 2020 -- Present}
	\item Created Golang libraries to communicate with a Minecraft server over
		\uhref{https://github.com/Coderlane/go-minecraft-rcon}{{RCON}},
		\uhref{https://github.com/Coderlane/go-minecraft-ping}{{Query}}, and via
		\uhref{https://github.com/Coderlane/go-minecraft-config}{{config files}}
	\item Implemented a
		\uhref{https://github.com/Coderlane/minecraft-sidecart}{{a daemon}} to
		upload Minecraft server metrics to Firebase's Firestore
	\item Working on
		\uhref{https://github.com/Coderlane/minecraft-sidecart-web}{{a website}}
		to display metrics and eventually manage the Minecraft server remotely
\end{position}

\begin{position}{GitHub Pull Request Annotator}{}{January 2016 -- May 2019}
	\item Designed and implemented while maintaining an integration branch to
		coordinate management of bugs, reviews, and product requirements to
		streamline development
	\item Annotates Pull Requests with metadata found in commit messages and
		stores metadata in a database for later lookups
	\item Analyzed over 200,000 commits with 20,000 bugs and 18,000 reviews
		in 2018
\end{position}

\begin{position}{Kernel Debugging Utilities}{}{June 2018 -- May 2019}
	\item Created a library of GDB debugging macros in Python
	\item Implemented over 20 KDB/DDB commands for kernel debugging; one was
		accepted by FreeBSD in
		\uhref{https://reviews.freebsd.org/D20219}{{D20219}}
\end{position}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{Accomplishments}

\begin{itemize} \itemsep -0.25em \vspace{0.25em}
	\item \uhref{https://patents.google.com/patent/US20200250039A1/en}{{Patent pending}} for modifications to
		distributed locking system
	\item Awarded Isilon Cluster Award for GitHub Pull Request Annotator
	\item Awarded Dell EMC Silver Award three times and EMC Gold Award once
\end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{Education}

\subsection*{Washington State University, Pullman, WA}
\begin{position}{B.S. Computer Science}{}{August 2011 -- May 2015}
\end{position}

\end{document}
