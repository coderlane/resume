.PHONY: all
all: pdf

.PHONY: pdf
pdf: resume.tex
	latexmk -pdf resume.tex

.PHONY: clean
clean:
	rm -f resume.aux resume.fdb_latexmk resume.fls resume.log resume.pdf
